// Copyright 2018 Andrei Bogdan Alexandru

#ifndef _SD_2018_DEQUE_H_
#define _SD_2018_DEQUE_H_

#pragma once
#include "Node.h"

template <typename T>
class Deque {
 private:
	int size;
	Node<T>* head;
	Node<T>* tail;
 public:
	Deque() : size(0), head(NULL), tail(NULL) {}
	~Deque();

	void push_back(T value);
	void push_front(T value);

	void pop_back();
	void pop_front();

	T front();
	T back();

	bool empty();
	int get_size();

	Node<T>* get_head();
	Node<T>* get_tail();
};

template<typename T>
Deque<T>::~Deque() {
	while (!this->empty()) {
		this->pop_front();
	}
}

template<typename T>
void Deque<T>::push_back(T value) {
	Node<T>* node = new Node<T>(value);
	if (this->empty()) {
		head = tail = node;
	} else {
		tail->set_next(node);
		node->set_prev(tail);
		tail = node;
	}
	size++;
}

template<typename T>
void Deque<T>::push_front(T value) {
	Node<T>* node = new Node<T>(value);
	if (this->empty()) {
		head = tail = node;
	} else {
		node->set_next(head);
		head->set_prev(node);
		head = node;
	}
	size++;
}

template<typename T>
void Deque<T>::pop_back() {
	if (this->empty()) {
		return;
	} else if (this->get_size() == 1) {
		delete tail;
		head = tail = NULL;
	} else {
		tail = tail->get_prev();
		delete (tail->get_next());
		tail->set_next(NULL);
	}
	size--;
}

template<typename T>
void Deque<T>::pop_front() {
	if (this->empty()) {
		return;
	} else if (this->get_size() == 1) {
		delete head;
		head = tail = NULL;
	} else {
		head = head->get_next();
		delete (head->get_prev());
		head->set_prev(NULL);
	}
	size--;
}

template<typename T>
T Deque<T>::front() {
	if (this->empty()) {
		return T();
	}
	return head->get_value();
}

template<typename T>
T Deque<T>::back() {
	if (this->empty()) {
		return T();
	}
	return tail->get_value();
}

template<typename T>
bool Deque<T>::empty() {
	return size == 0 || head == NULL || tail == NULL;
}

template<typename T>
int Deque<T>::get_size() {
	return size;
}

template<typename T>
Node<T>* Deque<T>::get_head() {
	return head;
}

template<typename T>
Node<T>* Deque<T>::get_tail() {
	return tail;
}
#endif  // _SD_2018_DEQUE_H_
