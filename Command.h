// Copyright 2018 Andrei Bogdan Alexandru

#pragma once
#ifndef _SD_2018_COMMAND_H_
#define _SD_2018_COMMAND_H_
#include <cstring>
#include <fstream>

using std::ofstream;
using std::iostream;
using std::endl;

/*
		Comanda pe care un robot o poate primi. In functie de tipul comenzii,
	vom avea un numar variabil de parametrii (setam cu default parametrii pe
	care ii asteptam)

		Type
	*** 1 = ADD_GET_BOX
	*** 2 = ADD_DROP_BOX
	*** 3 = EXECUTE
	*** 4 = PRINT_COMMANDS
	*** 5 = LAST_EXECUTED_COMMAND
	*** 6 = UNDO
	*** 7 = HOW_MUCH_TIME
	*** 8 = HOW_MANY_BOXES
*/
class Command {
 private:
	int RobotId;
	int type;
	int x, y;
	int NrBoxes;
	int time;  // Cat timp a petrecut in coada pana a fi executata
	int Priority;

 public:
	Command(const char cmd_text[] = "EMPTY_COMMAND", int RobotId = -1,
				int x = -1, int y = -1, int NrBoxes = -1, int Priority = 0) :
			RobotId(RobotId), type(command_type(cmd_text)), x(x), y(y),
			NrBoxes(NrBoxes), Priority(Priority), time(0) {}
	Command(const Command &cmd);
	~Command() {}

	// Printam comanda
	static int command_type(const char *cmd);
	int command_type();

	void print_command(ofstream &out);

	void increment_time();
	bool is_empty();
	int get_x();
	int get_y();
	int get_NrBoxes();
	int get_time();
	int get_robot_id();
	int get_priority();

	void set_NrBoxes(int NrBoxes);
	void set_time(int time);
};

Command::Command(const Command& cmd) {
	this->RobotId = cmd.RobotId;
	this->type = cmd.type;
	this->x = cmd.x;
	this->y = cmd.y;
	this->NrBoxes = cmd.NrBoxes;
	this->time = cmd.time;
	this->Priority = cmd.Priority;
}

int Command::command_type(const char* cmd) {
	if (strcmp(cmd, "EMPTY_COMMAND") == 0) {
		return 0;
	}
	if (strcmp(cmd, "ADD_GET_BOX") == 0) {
		return 1;
	}
	if (strcmp(cmd, "ADD_DROP_BOX") == 0) {
		return 2;
	}
	if (strcmp(cmd, "EXECUTE") == 0) {
		return 3;
	}
	if (strcmp(cmd, "PRINT_COMMANDS") == 0) {
		return 4;
	}
	if (strcmp(cmd, "LAST_EXECUTED_COMMAND") == 0) {
		return 5;
	}
	if (strcmp(cmd, "UNDO") == 0) {
		return 6;
	}
	if (strcmp(cmd, "HOW_MUCH_TIME") == 0) {
		return 7;
	}
	if (strcmp(cmd, "HOW_MANY_BOXES") == 0) {
		return 8;
	}
	return -1;
}

int Command::command_type() {
	return type;
}

void Command::print_command(ofstream &out) {
	switch (type) {
	case 1:
		out << "GET ";
		break;
	case 2:
		out << "DROP ";
		break;
	default:
		return;
	}
	out << x << " " << y << " " << NrBoxes;
}

void Command::increment_time() {
	time++;
}

bool Command::is_empty() {
	return type <= 0;
}

int Command::get_x() {
	return x;
}

int Command::get_y() {
	return y;
}

int Command::get_NrBoxes() {
	return NrBoxes;
}

int Command::get_time() {
	return time;
}

int Command::get_robot_id() {
	return RobotId;
}

int Command::get_priority() {
	return Priority;
}

void Command::set_NrBoxes(int NrBoxes) {
	this->NrBoxes = NrBoxes;
}

void Command::set_time(int time) {
	this->time = time;
}
#endif  // _SD_2018_COMMAND_H_
