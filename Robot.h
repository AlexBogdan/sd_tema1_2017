// Copyright 2018 Andrei Bogdan Alexandru

#ifndef _SD_2018_ROBOT_H_
#define _SD_2018_ROBOT_H_

#pragma once
#include <fstream>

#include "Command.h"
#include "Deque.h"
#include "Stack.h"

using std::endl;

class Robot
{
 private:
	int id;
	int NrBoxes;
	Deque<Command> commands;
	Deque<int> add_order_priorities;
	Command last_command;

 public:
	Robot() : id(-1), NrBoxes(0) {}
	explicit Robot(int id) : id(id), NrBoxes(0) {}
	~Robot() {}

	void add_command(Command cmd);
	void add_executed_command(Command cmd);
	void remove_command(int priority);
	bool has_commands();

	void print_commands(ofstream &out);
	void increment_cmd_time();

	int get_nrboxes();
	Command get_command();

	void pick_boxes(int NrBoxes);
	void drop_boxes(int NrBoxes);
};

void Robot::add_command(Command cmd) {
	if (cmd.get_priority() == 0) {
		commands.push_front(cmd);
	} else if (cmd.get_priority() == 1) {
		commands.push_back(cmd);
	}
}

void Robot::add_executed_command(Command cmd) {
	commands.push_front(cmd);
}

void Robot::remove_command(int priority) {
	if (priority == 0) {
		commands.pop_front();
	} else if (priority == 1) {
		commands.pop_back();
	}
}

bool Robot::has_commands() {
	return !commands.empty();
}

Command Robot::get_command() {
	if (commands.empty()) {
		return Command();
	}
	Command cmd = commands.front();
	commands.pop_front();

	return cmd;
}

void Robot::print_commands(ofstream &out) {
	if (!this->has_commands()) {
		out << "PRINT_COMMANDS: No command found" << endl;
		return;
	}

	out << "PRINT_COMMANDS: " << this->id << ": ";
	auto it = commands.get_head();
	for ( ; it != commands.get_tail(); it = it->get_next()) {
		it->get_value().print_command(out);
		out << "; ";
	}
	it->get_value().print_command(out);
	out << endl;
}

void Robot::increment_cmd_time() {
	if (commands.empty()) return;

	for (auto it = commands.get_head(); it != NULL; it = it->get_next()) {
		it->get_value().increment_time();
	}
}

int Robot::get_nrboxes() {
	return NrBoxes;
}

void Robot::pick_boxes(int NrBoxes) {
	this->NrBoxes += NrBoxes;
}

void Robot::drop_boxes(int NrBoxes) {
	this->NrBoxes -= NrBoxes;
}
#endif  // _SD_2018_ROBOT_H_
