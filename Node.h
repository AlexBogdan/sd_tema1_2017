// Copyright 2018 Andrei Bogdan Alexandru

#ifndef _SD_2018_NODE_H_
#define _SD_2018_NODE_H_

#pragma once

template <typename T>
class Node
{
 private:
	T value;
	Node* prev;
	Node* next;
 public:
	Node() : prev(NULL), next(NULL) {}
	explicit Node(T value) : prev(NULL), next(NULL), value(value) {}
	~Node() {}

	void set_value(T value);
	void set_next(Node *next);
	void set_prev(Node *prev);

	T& get_value();
	Node* get_next();
	Node* get_prev();
};

template<typename T>
void Node<T>::set_value(T value) {
	this->value = value;
}

template<typename T>
void Node<T>::set_next(Node<T>* next) {
	this->next =  next;
}

template<typename T>
void Node<T>::set_prev(Node<T>* prev) {
	this->prev = prev;
}

template<typename T>
T& Node<T>::get_value() {
	return value;
}

template<typename T>
Node<T>* Node<T>::get_next() {
	return next;
}

template<typename T>
Node<T>* Node<T>::get_prev() {
	return prev;
}
#endif  //  _SD_2018_NODE_H_
