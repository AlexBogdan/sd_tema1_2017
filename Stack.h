// Copyright 2018 Andrei Bogdan Alexandru

#ifndef _SD_2018_STACK_H_
#define _SD_2018_STACK_H_

#pragma once
#include "Node.h"

template<typename T>
class Stack {
 private:
	int size;
	Node<T>* head;
 public:
	Stack() : size(0), head(NULL) {}
	~Stack();

	void push(T value);
	void pop();
	T top();

	int get_size();
	bool empty();
};

template<typename T>
Stack<T>::~Stack() {
	while (!this->empty()) {
		this->pop();
	}
}

template<typename T>
void Stack<T>::push(T value) {
	Node<T>* node = new Node<T>(value);
	if (this->empty()) {
		head = node;
	} else {
		head->set_next(node);
		node->set_prev(head);
		head = node;
	}
	size++;
}

template<typename T>
void Stack<T>::pop() {
	if (this->empty()) {
		return;
	} else if (this->get_size() == 1) {
		delete head;
	} else {
		head = head->get_prev();
		delete head->get_next();
	}
	size--;
}

template<typename T>
T Stack<T>::top() {
	if (this->empty()) {
		return T();
	}
	return head->get_value();
}

template<typename T>
int Stack<T>::get_size() {
	return size;
}

template<typename T>
bool Stack<T>::empty() {
	return size == 0;
}
#endif  // _SD_2018_STACK_H_
