// Copyright 2018 Andrei Bogdan Alexandru

#include <iostream>
#include <fstream>

#include "Command.h"
#include "Robot.h"
#include "Node.h"
#include "Stack.h"

using std::ifstream;
using std::ofstream;
using std::endl;


ifstream in("robots.in");
ofstream out("robots.out");

int NrRobots;
int n, m;  // LIN COL a depozitului
int **deposit;

Robot *robots;
Stack<Command> executed_commands;

void execute_command(Command exec_cmd) {
	// Daca robotul nu are comenzi de executat, iesim
	if (!robots[exec_cmd.get_robot_id()].has_commands()) {
		out << "EXECUTE: No command to execute" << endl;
		return;
	}

	Command cmd = robots[exec_cmd.get_robot_id()].get_command();
	if (cmd.command_type() == 1) {
		// Ne asiguram ca robotul nu va lua mai multe comenzi decat exista
		int available_boxes = deposit[cmd.get_x()][cmd.get_y()];
		if (available_boxes >= cmd.get_NrBoxes()) {
			available_boxes = cmd.get_NrBoxes();
		}
		// Robotul ia cutiile din depozit
		robots[cmd.get_robot_id()].pick_boxes(available_boxes);
		deposit[cmd.get_x()][cmd.get_y()] -= available_boxes;
		cmd.set_NrBoxes(available_boxes);
	} else if (cmd.command_type() == 2) {
		// Ne asiguram ca robotul are numarul necesar de cutii
		int available_boxes = robots[cmd.get_robot_id()].get_nrboxes();
		if (available_boxes >= cmd.get_NrBoxes()) {
			available_boxes = cmd.get_NrBoxes();
		}
		// Robotul lasa cutiile in depozit
		robots[cmd.get_robot_id()].drop_boxes(available_boxes);
		deposit[cmd.get_x()][cmd.get_y()] += available_boxes;
		cmd.set_NrBoxes(available_boxes);
	}

	executed_commands.push(cmd);
	executed_commands.push(exec_cmd);
}

Command last_command() {
	// Daca nu avem nicio comanda executata, intoarcem un Command gol
	if (executed_commands.empty()) {
		return Command();
	}

	Command cmd = executed_commands.top();
	// 	Daca last_command a fost un Execute, intoarcem
	// comanda de dinaintea acesteia
	if (cmd.command_type() == 3) {
		Command exec = cmd;
		executed_commands.pop();
		cmd = executed_commands.top();
		executed_commands.push(exec);
	}

	return cmd;
}

Command last_executed_command() {
	Stack<Command> s;
	Command cmd;
	while (!executed_commands.empty() && cmd.command_type() != 3) {
		cmd = executed_commands.top();
		s.push(cmd);
		executed_commands.pop();
	}

	if (!executed_commands.empty()) {
		cmd = executed_commands.top();
	} else {
		cmd = Command();
	}

	while (!s.empty()) {
		executed_commands.push(s.top());
		s.pop();
	}

	return cmd;
}


void undo_command() {
	// Verificam care este ultima comanda ADD sau EXECUTE
	Command cmd = last_command();
	if (cmd.command_type() == 0) {
		out << "UNDO: No History" << endl;
		return;
	}
	// 	Verificam comanda din varful stivei de comenzi executate
	// 	Daca ultima cmd a fost ADD_GET_BOX sau ADD_DROP_BOX,
	// doar o scoate din lista robotului
	// 	Daca ultima a fost un EXECUTE,
	// atunci va trebui sa facem Undo pentru efectul comenzii
	if (executed_commands.top().command_type() == 1 ||
		executed_commands.top().command_type() == 2) {
		robots[cmd.get_robot_id()].remove_command(cmd.get_priority());
		executed_commands.pop();
		return;
	} else if (executed_commands.top().command_type() == 3) {
		if (cmd.command_type() == 1) {
			robots[cmd.get_robot_id()].drop_boxes(cmd.get_NrBoxes());
			deposit[cmd.get_x()][cmd.get_y()] += cmd.get_NrBoxes();
		} else if (cmd.command_type() == 2) {
			robots[cmd.get_robot_id()].pick_boxes(cmd.get_NrBoxes());
			deposit[cmd.get_x()][cmd.get_y()] -= cmd.get_NrBoxes();
		}
		// Adaugam comanda inapoi in lista robotului
		cmd.set_time(0);
		robots[cmd.get_robot_id()].add_executed_command(cmd);

		executed_commands.pop();
		executed_commands.pop();
	}
}

void count_time() {
	for (int i = 0; i < NrRobots; ++i) {
		robots[i].increment_cmd_time();
	}
}

void print_map() {
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j) {
			out << deposit[i][j] << " ";
		}
		out << endl;
	}
}


int main() {
	// Citim datele de baza + harta depozitului
	in >> NrRobots >> n >> m;

	robots = new Robot[NrRobots];
	deposit = new int*[n];
	for (int i = 0; i < n; ++i) {
		deposit[i] = new int[m];
	}

	for (int i = 0; i < NrRobots; ++i) {
		robots[i] = Robot(i);
	}

	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j) {
			in >> deposit[i][j];
		}
	}

	// Citim comenziile pe care le dam robotilor
	char cmd_text[32];  // Buffer pentru citire nume comanda
	int RobotId, x, y, NrBoxes, Priority;  // Parametri ce mai pot aparea
	Command cmd;
	while (in >> cmd_text) {
		switch (Command::command_type(cmd_text)) {
		case 1:  // ADD_GET_BOX
		case 2:  // ADD_DROP_BOX
			in >> RobotId >> x >> y >> NrBoxes >> Priority;
			cmd = Command(cmd_text, RobotId, x, y, NrBoxes, Priority);
			robots[RobotId].add_command(cmd);
			executed_commands.push(cmd);
			break;
		case 5:  // LAST_EXECUTED_COMMAND
			cmd = last_executed_command();
			if (cmd.command_type() == 0) {
				out << "LAST_EXECUTED_COMMAND: No command was executed" << endl;
			} else {
				out << "LAST_EXECUTED_COMMAND: " << cmd.get_robot_id() << ": ";
				cmd.print_command(out);
				out << endl;
			}
			break;
		case 6:  // UNDO
			undo_command();
			break;
		case 3:  // EXECUTE
			in >> RobotId;
			cmd = Command(cmd_text, RobotId);
			execute_command(cmd);
			break;
		case 4:  // PRINT_COMMANDS
			in >> RobotId;
			robots[RobotId].print_commands(out);
			break;
		case 7:  // HOW_MUCH_TIME
			cmd = last_executed_command();
			if (cmd.command_type() == 0) {
				out << "HOW_MUCH_TIME: No command was executed" <<  endl;
			} else {
				out << "HOW_MUCH_TIME: " << cmd.get_time() << endl;
			}
			break;
		case 8:  // HOW_MANY_BOXES
			in >> RobotId;
			out << "HOW_MANY_BOXES: " << robots[RobotId].get_nrboxes() << endl;
			break;
		}
		count_time();
	}

	return 0;
}
