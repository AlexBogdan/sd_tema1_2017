all: build clean

CC=g++

build: tema1

tema1: Source.cpp Command.h Robot.h Node.h Stack.h Deque.h
	$(CC) $< -o $@

clean:
	rm tema1 *.o
